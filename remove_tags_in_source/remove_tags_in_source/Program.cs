﻿using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace remove_tags_in_source
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.InputEncoding = Encoding.Unicode;
            Console.OutputEncoding = Encoding.Unicode;

            Console.WriteLine("Copy and paste file .mqxliff file which you want the tags removed");
            string file = Console.ReadLine();

            file = file.Replace("\"", "");

            string alltext = File.ReadAllText(file);

            string cleaned_text = Regex.Replace(alltext, "<bpt .*?/bpt>", "");
            cleaned_text = Regex.Replace(cleaned_text, "<ept .*?/ept>", "");
            cleaned_text = Regex.Replace(cleaned_text, "<ph .*?/ph>", "");

            File.WriteAllText(file.Replace(".mqxliff", "_updated.mqxliff"),cleaned_text);

            Console.WriteLine("Done!");
            Console.ReadKey();
        }
    }
}
